package shell.bin;
import utils.*;
import shell.shell;

public class Mv {

    public Mv(){}
    public Mv(shell s , String[] argv)
    {
        if(! checkArg(argv))
        {
            System.out.println("Invalid Amount of Arguemnt ");
            return;
        }
        if(argv[1].charAt(argv[1].length()-1)!='/')
        {
            argv[1] +="/"; 
        }
        if(argv[2].charAt(argv[2].length()-1)!='/')
        {
            argv[2] +="/"; 
        }

        Directory src = Directory.resolvePath(s, s.cwd, argv[1]);
        Directory dest = Directory.resolvePath(s, s.cwd, argv[2]);

        if(dest.getFlag()!= 1)
        {
            System.out.println("Can't move to this location ");
            return;
        }

        Directory.delete(src);
        dest.add(src);
        
        return;
    }
    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 3)
         return false;
        return true;
    }
}