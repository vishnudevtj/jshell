package shell.bin;
import utils.*;
import shell.shell;

public class Rm {

    public Rm(){}
    public Rm(shell s , String[] argv)
    {
        if(! checkArg(argv))
        {
            System.out.println("Invalid Amount of Arguemnt ");
            return;
        }
        if(argv[1].charAt(argv[1].length()-1)!='/')
        {
            argv[1] +="/"; 
        }
        Directory node = Directory.resolvePath(s, s.cwd, argv[1]);
        Directory.delete(node);
        return;
    }
    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 2)
         return false;
        return true;
    }
}
