package utils;

public class Inode {
	
	protected int flag;
	
	public Inode next;
	public Inode parent;
	public Inode child;

	protected String name;

	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return name;
	}

	public int getFlag()
	{
		return this.flag;
	}

}